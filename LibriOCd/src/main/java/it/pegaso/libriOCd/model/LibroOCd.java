package it.pegaso.libriOCd.model;

public class LibroOCd {

	private int id;
	private String titolo;
	private String autore;
	private String genere;
	private String anno;
	public int isLibroOrdCd;
	public int quantita;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getTitolo() {
		return titolo;
	}
	public void setTitolo(String titolo) {
		this.titolo = titolo;
	}
	public String getAutore() {
		return autore;
	}
	public void setAutore(String autore) {
		this.autore = autore;
	}
	public String getGenere() {
		return genere;
	}
	public void setGenere(String genere) {
		this.genere = genere;
	}
	public String getAnno() {
		return anno;
	}
	public void setAnno(String anno) {
		this.anno = anno;
	}
	public int getIsLibroOrdCd() {
		return isLibroOrdCd;
	}
	public void setIsLibroOrdCd(int isLibroOrdCd) {
		this.isLibroOrdCd = isLibroOrdCd;
	}
	public int getQuantita() {
		return quantita;
	}
	public void setQuantita(int quantita) {
		this.quantita = quantita;
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("Articolo: ");
		sb.append(getTitolo());
		sb.append(" by: ");
		sb.append(getAutore());
		return sb.toString();
	}
}
