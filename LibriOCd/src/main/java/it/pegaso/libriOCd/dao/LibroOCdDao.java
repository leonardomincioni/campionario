package it.pegaso.libriOCd.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import it.pegaso.libriOCd.model.LibroOCd;

public class LibroOCdDao {
	
	private final static String CONNECTION_STRING = "jdbc:mysql://localhost:3306/librocd?user=root&password=root";
	
	private Connection connection;
	private PreparedStatement getLibroById;
	private PreparedStatement getCdById;
	
	public LibroOCd getLibro(int id) {
		LibroOCd result = null;

		try {
			getGetLibroById().clearParameters();
			
			getGetLibroById().setInt(1, id);
			ResultSet rs = getGetLibroById().executeQuery();
			
			if (rs.next()) {
				result = new LibroOCd();
				
				result.setId(rs.getInt("id"));
				result.setTitolo(rs.getString("titolo"));
				result.setAutore(rs.getString("autore"));
				result.setGenere(rs.getString("genere"));
				result.setAnno(rs.getString("anno"));
				result.setIsLibroOrdCd(rs.getInt("isLibroOrCd"));
				result.setQuantita(rs.getInt("quantita"));
			}
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return result;
	}
	
	public List<LibroOCd> getall(){
		List<LibroOCd> result = new ArrayList<LibroOCd>();
		try {
			ResultSet rs = getConnection().createStatement().executeQuery("select * from campionario");
			while(rs.next()) {
				LibroOCd l = new LibroOCd();
				l.setId(rs.getInt("id"));
				l.setTitolo(rs.getString("titolo"));
				l.setAutore(rs.getString("autore"));
				l.setGenere(rs.getString("genere"));
				l.setAnno(rs.getString("anno"));
				l.setIsLibroOrdCd(rs.getInt("isLibroOrCd"));
				l.setQuantita(rs.getInt("quantita"));
				
				result.add(l);
				
			}
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return result;
	}
	
	public Connection getConnection() throws SQLException, ClassNotFoundException {
		if (connection == null) {
			Class.forName("com.mysql.cj.jdbc.Driver");
			connection = DriverManager.getConnection(CONNECTION_STRING);
		}
		return connection;
	}
	public void setConnection(Connection connection) {
		this.connection = connection;
	}
	public PreparedStatement getGetLibroById() throws ClassNotFoundException, SQLException {
		if(getLibroById == null) {
			getLibroById = getConnection().prepareStatement("select * from campionario where id=? and isLibroOrCd=0");
		}
		return getLibroById;
	}
	public void setGetLibroById(PreparedStatement getLibroById) {
		this.getLibroById = getLibroById;
	}
	public PreparedStatement getGetCdById() {
		return getCdById;
	}
	public void setGetCdById(PreparedStatement getCdById) {
		this.getCdById = getCdById;
	}
	public static String getConnectionString() {
		return CONNECTION_STRING;
	}
	
	

}
