package it.pegaso.libriOCd.business;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import it.pegaso.libriOCd.dao.LibroOCdDao;
import it.pegaso.libriOCd.model.LibroOCd;

/**
 * Servlet implementation class LibroById
 */
public class LibroById extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public LibroById() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		LibroOCdDao dao = new LibroOCdDao();
		LibroOCd lcd = new LibroOCd();
		
		String index = request.getParameter("id");
		
		int id = Integer.parseInt(index);
		lcd = dao.getLibro(id);
		
		request.setAttribute("articolo", lcd);
		
		RequestDispatcher rd = request.getRequestDispatcher("libroCercato.jsp");
		rd.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
