<%@page import="it.pegaso.libriOCd.model.LibroOCd"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Ricerca libro</title>
</head>
<body style="background-color: cyan;">

	<% LibroOCd lcd = (LibroOCd) request.getAttribute("articolo");
	
	if(lcd == null){
	%>

	<h1>Libro non trovato</h1>
	<% } else { %>
		<p><%= lcd.toString() %></p>
	<% } %>


</body>
</html>