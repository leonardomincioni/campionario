<%@page import="it.pegaso.libriOCd.model.LibroOCd"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Catalogo Libri</title>
</head>
<body>
	<h1 style="color: red; text-align: center; font-weight: bold;"> Ecco i nostri libri</h1>
	<table align="center">
		<thead>
			<tr>
				<th style="width: 20em; border: 1px solid black;">Titolo</th>
				<th style="width: 20em; border: 1px solid black;">Autore</th>
				<th style="width: 20em; border: 1px solid black;">Genere</th>
			</tr>
		</thead>
		<tbody>
			<% List<LibroOCd> list= (List<LibroOCd>)request.getAttribute("list"); %>
			<%
				for(LibroOCd lcd: list){
					
					if(lcd.getIsLibroOrdCd() == 0){				
			%>
				<tr>
					<td style="width: 20em; border: 1px solid black; text-align: center;"><%= lcd.getTitolo()%></td>
					<td style="width: 20em; border: 1px solid black; text-align: center;"><%=lcd.getAutore()%></td>
					<td style="width: 20em; border: 1px solid black; text-align: center;"><%= lcd.getGenere()%></td>
				</tr>
			
			<%
					}
				}
			%>
			
		</tbody>
	</table>
	
	<form action="LibroById">
		<p>Inserire l'id del libro da cercare</p>
		<input type="text" name="id">
		<input type="submit" value="cerca">
	</form>
	
</body>
</html>